package com.example.msgsender.commandmsgsender;

import com.example.client.ClientHandler;
import com.example.msgsender.MessageSender;
import com.example.statistics.StatisticsInfo;

public interface StatisticsCommandMessageSender extends MessageSender {
    default void sendStatisticsMessage(ClientHandler clientHandler) {
        StatisticsInfo statistics = new StatisticsInfo(getClientHandlers());
        sendMessage(clientHandler, statistics.toString());
    }
}
