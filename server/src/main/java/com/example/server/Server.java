package com.example.server;

import com.example.Constants;
import com.example.client.ClientHandler;
import com.example.client.ClientRunnable;
import com.example.handler.InactivityHandler;
import com.example.msgsender.facade.MessageSenderFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

public class Server {
    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private final int port;
    private final long inactivityTimeout;
    private final int terminationTimeout;

    private ServerSocket serverSocket;
    private final ExecutorService executorService;

    private final Set<ClientHandler> clients = ConcurrentHashMap.newKeySet();
    private final Set<ClientHandler> unmodifiableClients = Collections.unmodifiableSet(clients);

    private boolean keepRunning = true;

    private Server(ServerBuilder builder) {
        LOGGER.info("Try to create Server.");
        this.port = builder.port;
        this.inactivityTimeout = builder.inactivityTimeout;
        this.terminationTimeout = builder.terminationTimeout;
        this.executorService = builder.executorService;
    }

    public void start() {
        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setSoTimeout(terminationTimeout);
        } catch (IOException e) {
            LOGGER.error("Exception was thrown while the server socket was created:", e);
            this.keepRunning = false;
        }
        if (keepRunning) {
            LOGGER.info("The server is accepting connections on port {}", serverSocket.getLocalPort());
            InactivityHandler inactivityHandler = new InactivityHandler(this);
            executorService.execute(inactivityHandler);
            MessageSenderFacade messageSenderFacade = new MessageSenderFacade(this::getClients, this::unregisterClient);
            while (keepRunning) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    LOGGER.info("The server has accepted the connection from the client port {}", clientSocket.getPort());
                    ClientRunnable clientRunnable = new ClientRunnable(clientSocket, messageSenderFacade, this::stopClient);
                    executorService.execute(clientRunnable);
                    registerClient(clientRunnable.getClientHandler());
                } catch (SocketTimeoutException e) {
                    LOGGER.error("The server will shutdown due to termination timeout!");
                    break;
                } catch (IOException e) {
                    LOGGER.error("The exception was thrown while accepting connection from the client:", e);
                    break;
                }
            }
        }
        closeAndStop();
        LOGGER.info("The server stopped accepting new connections.");
    }

    public void closeAndStop() {
        if (!keepRunning) {
            return;
        }
        LOGGER.info("Server is trying to shutdown.");
        this.keepRunning = false;
        stopClients();
        executorService.shutdownNow();
        try {
            serverSocket.close();
        } catch (IOException e) {
            LOGGER.error("The exception was thrown while closing the server socket: ", e);
        }
    }

    private void stopClients() {
        LOGGER.info("Server is trying to stop all the clients...");
        for (ClientHandler clientHandler : clients) {
            stopClient(clientHandler);
        }
        LOGGER.info("Server has stopped all the clients...");
    }

    private void stopClient(ClientHandler clientHandler) {
        LOGGER.info("Server is trying to stop the client: {}", clientHandler.getClientInfo().getID());
        clientHandler.disconnect();
        unregisterClient(clientHandler);
        LOGGER.info("Server has stopped the client: {}", clientHandler.getClientInfo().getID());
    }

    public void unregisterClient(ClientHandler clientHandler) {
        clients.remove(clientHandler);
    }

    private void registerClient(ClientHandler clientHandler) {
        clients.add(clientHandler);
    }

    public Set<ClientHandler> getClients() {
        return unmodifiableClients;
    }

    public boolean isKeepRunning() {
        return keepRunning;
    }

    public int getPort() {
        return port;
    }

    public long getInactivityTimeout() {
        return inactivityTimeout;
    }

    public long getTerminationTimeout() {
        return terminationTimeout;
    }

    public static class ServerBuilder {
        private int port = Constants.DEFAULT_SERVER_PORT;
        private long inactivityTimeout = Constants.ACCEPTABLE_INACTIVITY_DURATION_SEC;
        private int terminationTimeout = Constants.DEFAULT_TERMINATION_TIMEOUT_MS;
        private ExecutorService executorService = Constants.DEFAULT_EXECUTOR_SERVICE;

        public ServerBuilder port(int port) {
            this.port = port;
            return this;
        }

        public ServerBuilder inactivityTimeout(long inactivityTimeout) {
            this.inactivityTimeout = inactivityTimeout;
            return this;
        }

        public ServerBuilder terminationTimeout(int terminationTimeout) {
            this.terminationTimeout = terminationTimeout;
            return this;
        }

        public ServerBuilder executorService(ExecutorService executorService) {
            this.executorService = executorService;
            return this;
        }

        public Server build() {
            return new Server(this);
        }
    }
}
