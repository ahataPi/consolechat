package com.example.msgsender;

import com.example.client.ClientHandler;
import com.example.client.ClientInfo;
import com.example.message.Message;

public interface MessageSender extends ClientHandlerAware {

    default void broadcastMessage(Message message) {
        if (message.getAuthor() != null) {
            for (ClientHandler client : getClientHandlers()) {
                ClientInfo clientInfo = client.getClientInfo();
                if (!clientInfo.equals(message.getAuthor().getClientInfo()) && clientInfo.isLoggedIn()) {
                    sendMessage(client, message.getText());
                }
            }
        }
    }

    default void sendMessage(ClientHandler clientHandler, String message) {
        clientHandler.sendMessage(message);
    }
}
