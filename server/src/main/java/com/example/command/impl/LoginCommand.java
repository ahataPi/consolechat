package com.example.command.impl;

import com.example.client.ClientHandler;
import com.example.client.ClientInfo;
import com.example.command.Command;
import com.example.message.Message;
import com.example.msgsender.facade.MessageSenderFacade;

import java.util.Set;

public class LoginCommand extends Command {

    public LoginCommand(String commandCode) {
        super(commandCode);
    }

    @Override
    public void execute(Message message, MessageSenderFacade messageSenderFacade) {
        String[] messageParts = message.getText().split(" ", 3);
        ClientHandler client = message.getAuthor();
        ClientInfo clientInfo = client.getClientInfo();
        if (clientInfo.isLoggedIn()) {
            messageSenderFacade.sendAlreadyLoggedIn(client);
            return;
        }
        String potentialNickname = ((messageParts.length != 1) ? messageParts[1] : "").trim();
        if (!checkNicknameValidity(potentialNickname, messageSenderFacade.getClientHandlers())) {
            messageSenderFacade.warnChangeNickname(client, potentialNickname);
            return;
        }
        clientInfo.setNickName(potentialNickname);
        clientInfo.setLoggedIn(true);
        messageSenderFacade.congratsWithRegistration(client);
        messageSenderFacade.greetNewClient(client);
    }

    private boolean checkNicknameValidity(String nickname, Set<ClientHandler> clientHandlers) {
        boolean isOriginalNickname = clientHandlers
                .stream()
                .map(ClientHandler::getClientInfo)
                .noneMatch(clientInfo -> nickname.equals(clientInfo.getNickName()));
        return !nickname.isEmpty() && isOriginalNickname;
    }
}
