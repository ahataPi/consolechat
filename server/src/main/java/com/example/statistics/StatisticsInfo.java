package com.example.statistics;

import com.example.client.ClientHandler;
import com.example.client.ClientInfo;

import java.util.Set;
import java.util.stream.Collectors;

public class StatisticsInfo {
    private final int clientsNumber;
    private final Set<ClientStatisticsInfo> connectedClients;
    private final long messageNumber;

    public StatisticsInfo(Set<ClientHandler> connectedClients) {
        this.messageNumber = connectedClients.stream()
                .map(ClientHandler::getClientInfo)
                .filter(ClientInfo::isLoggedIn)
                .mapToLong(ClientInfo::getMessagesNumber)
                .sum();
        this.connectedClients = connectedClients.stream()
                .map(ClientHandler::getClientInfo)
                .filter(ClientInfo::isLoggedIn)
                .map(ClientStatisticsInfo::new)
                .collect(Collectors.toSet());
        this.clientsNumber = this.connectedClients.size();
    }

    @Override
    public String toString() {
        return "CHAT STATISTICS: \n" +
                "Number of clients: " + clientsNumber + "\n" +
                "Total number of messages: " + messageNumber + "\n" +
                "List of connected users: \n" + connectedClients;
    }
}
