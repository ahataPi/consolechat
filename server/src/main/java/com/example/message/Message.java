package com.example.message;

import com.example.client.ClientHandler;

public class Message {
    private final String text;
    private final ClientHandler author;

    public Message(String text, ClientHandler author) {
        this.text = text;
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public ClientHandler getAuthor() {
        return author;
    }
}