package com.example.client;

import com.example.connector.SocketServerConnector;
import com.example.inputprocessor.ConsoleInputProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Client {
    private static final Logger logger = LoggerFactory.getLogger(Client.class);

    private final int port;
    private final String hostName;
    private boolean isActive = false;

    private final SocketServerConnector serverConnector = new SocketServerConnector();
    private final ConsoleInputProcessor inputProcessor = new ConsoleInputProcessor();

    public Client(int port, String hostName) {
        this.port = port;
        this.hostName = hostName;
    }

    public void start() {
        connectToServer();
        while (this.isActive) {
            sendMessage();
            showMessage();
        }
        disconnectFromServer();
    }

    private void connectToServer() {
        logger.info("The client tries to connect to Server.");
        try {
            serverConnector.connect(port, hostName);
            this.isActive = true;
            logger.info("The client has connected to the Server.");
        } catch (IOException e) {
            logger.error("The client can't connect to server.");
            disconnectFromServer();
        }
    }

    private void sendMessage() {
        String clientMessage = inputProcessor.readMessage();
        if (clientMessage != null && !clientMessage.isEmpty()) {
            serverConnector.sendMessage(clientMessage);
        }
    }

    private void showMessage() {
        String message = null;
        try {
            message = serverConnector.readMessage();
        } catch (IOException e) {
            logger.error("The client can't receive messages from the server.");
            disconnectFromServer();
        }
        if (message != null) {
            System.out.println(message);
        }
    }

    private void disconnectFromServer() {
        logger.info("The client tries to disconnect from Server.");
        serverConnector.disconnect();
        this.isActive = false;
        logger.info("The client has disconnected from the Server.");
    }
}
