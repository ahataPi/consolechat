package com.example.executor;

import com.example.client.ClientHandler;
import com.example.command.factory.CommandFactory;
import com.example.message.Message;
import com.example.msgsender.facade.MessageSenderFacade;
import com.example.resolver.CommandResolver;

public class CommandExecutor {
    private final ClientHandler clientHandler;
    private final MessageSenderFacade messageSenderFacade;
    private final CommandResolver commandResolver;

    public CommandExecutor(ClientHandler clientHandler, MessageSenderFacade messageSenderFacade) {
        this.clientHandler = clientHandler;
        this.messageSenderFacade = messageSenderFacade;
        this.commandResolver = new CommandResolver(clientHandler, messageSenderFacade);
    }

    public void executeCommand(String textMessage) {
        if (commandResolver.isCommandMessage(textMessage)) {
            CommandFactory commandFactory = commandResolver.resolveCommandCode(textMessage);
            if (commandFactory != null) {
                commandFactory.getCommand().execute(new Message(textMessage, clientHandler), messageSenderFacade);
            }
        } else {
            messageSenderFacade.broadcastClientMsg(textMessage, clientHandler);
            if (clientHandler.getClientInfo().isLoggedIn()) {
                clientHandler.getClientInfo().incrementMessageNumber();
            }
        }
    }
}
