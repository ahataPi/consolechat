package com.example.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.time.LocalDateTime;

public class ClientHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientHandler.class);

    private final Socket clientSocket;
    private BufferedReader in;
    private PrintWriter out;

    private final ClientInfo clientInfo = new ClientInfo();

    public ClientHandler(Socket socket) {
        clientSocket = socket;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        } catch (IOException e) {
            LOGGER.error("The exception was thrown while creating input and output streams for the client, " +
                    "reading and sending messages is impossible: ", e);
            disconnect();
        }
    }

    public String readMessage() {
        String message = null;
        try {
            message = in.readLine();
        } catch (IOException e) {
            //ignored
        }
        if (message != null && !message.trim().isEmpty()) {
            clientInfo.setLastMessageTime(LocalDateTime.now());
        }
        return message;
    }

    public void sendMessage(String message) {
        out.println(message);
    }

    public void disconnect() {
        stopSending();
        stopReading();
        closeSocket();
    }

    private void stopSending() {
        if (out != null) {
            out.close();
        }
    }

    private void stopReading() {
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException e) {
            LOGGER.error("The exception was thrown while closing the input stream for the client: ", e);
        }
    }

    private void closeSocket() {
        if (clientSocket != null) {
            try {
                clientSocket.close();
            } catch (IOException e) {
                LOGGER.error("The exception was thrown while closing the client socket: ", e);
            }
        }
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}

