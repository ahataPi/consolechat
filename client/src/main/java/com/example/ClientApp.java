package com.example;

import com.example.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientApp.class);

    public static void main(String[] args) {
        Client client = new Client(Constants.DEFAULT_SERVER_PORT, Constants.DEFAULT_SERVER_HOST);
        LOGGER.info("The client is starting...");
        client.start();
    }
}
