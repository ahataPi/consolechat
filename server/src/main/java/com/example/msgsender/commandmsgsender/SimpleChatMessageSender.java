package com.example.msgsender.commandmsgsender;

import com.example.client.ClientHandler;
import com.example.message.Message;
import com.example.msgsender.MessageSender;

public interface SimpleChatMessageSender extends MessageSender {
    String SIMPLE_CLIENT_MESSAGE = "[%s]:%s";
    String CANT_SEND_MSG_MESSAGE = "You can't send messages in chat before login. Use /login [nickname] command.";

    default void broadcastClientMsg(String messageText, ClientHandler clientSender) {
        if (clientSender.getClientInfo().isLoggedIn()) {
            String msgText = String.format(SIMPLE_CLIENT_MESSAGE, clientSender.getClientInfo().getNickName(), messageText);
            Message message = new Message(msgText, clientSender);
            broadcastMessage(message);
        } else {
            sendMessage(clientSender, CANT_SEND_MSG_MESSAGE);
        }
    }
}
