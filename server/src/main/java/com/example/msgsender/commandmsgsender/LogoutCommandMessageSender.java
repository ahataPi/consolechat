package com.example.msgsender.commandmsgsender;

import com.example.client.ClientHandler;
import com.example.message.Message;
import com.example.msgsender.MessageSender;

public interface LogoutCommandMessageSender extends MessageSender {
    String DISCONNECTION_OF_CLIENT_MESSAGE = "Unfortunately, the client %s has disconnected.";
    String CANT_LOGOUT_MESSAGE = "You can't logout from chat before login. Use /login [nickname] command.";

    default void cantLogoutBeforeLogin(ClientHandler clientHandler) {
        sendMessage(clientHandler, CANT_LOGOUT_MESSAGE);
    }

    default void notifyDisconnectionOfClient(ClientHandler clientHandler) {
        Message message = new Message(String.format(DISCONNECTION_OF_CLIENT_MESSAGE, clientHandler.getClientInfo().getNickName()), clientHandler);
        broadcastMessage(message);
    }
}
