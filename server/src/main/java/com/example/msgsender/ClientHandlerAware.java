package com.example.msgsender;

import com.example.client.ClientHandler;

import java.util.Set;

public interface ClientHandlerAware {
    Set<ClientHandler> getClientHandlers();
    void unregisterClient(ClientHandler clientHandler);
}
