package com.example.command.impl;

import com.example.client.ClientHandler;
import com.example.command.Command;
import com.example.message.Message;
import com.example.msgsender.facade.MessageSenderFacade;

public class LogoutCommand extends Command {
    public LogoutCommand(String commandCode) {
        super(commandCode);
    }

    @Override
    public void execute(Message message, MessageSenderFacade messageSenderFacade) {
        ClientHandler clientHandler = message.getAuthor();
        if (clientHandler.getClientInfo().isLoggedIn()) {
            clientHandler.disconnect();
            messageSenderFacade.unregisterClient(clientHandler);
        } else {
            messageSenderFacade.cantLogoutBeforeLogin(clientHandler);
        }
    }
}
