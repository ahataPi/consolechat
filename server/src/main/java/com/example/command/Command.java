package com.example.command;

import com.example.message.Message;
import com.example.msgsender.facade.MessageSenderFacade;

public abstract class Command {
    private final String commandCode;

    public Command(String commandCode) {
        this.commandCode = commandCode;
    }

    public abstract void execute(Message message, MessageSenderFacade messageSenderFacade);

    public String getCommandCode() {
        return commandCode;
    }
}
