package com.example.msgsender.facade;

import com.example.client.ClientHandler;
import com.example.msgsender.commandmsgsender.*;

import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class MessageSenderFacade implements
        LoginCommandMessageSender,
        LogoutCommandMessageSender,
        HelpCommandMessageSender,
        ConfigServerMessageSender,
        SimpleChatMessageSender,
        StatisticsCommandMessageSender {

    private final Supplier<Set<ClientHandler>> getClientsInfos;
    private final Consumer<ClientHandler> unregisterClients;

    public MessageSenderFacade(Supplier<Set<ClientHandler>> getClientsInfos, Consumer<ClientHandler> unregisterClients) {
        this.getClientsInfos = getClientsInfos;
        this.unregisterClients = unregisterClients;
    }

    @Override
    public Set<ClientHandler> getClientHandlers() {
        return getClientsInfos.get();
    }

    @Override
    public void unregisterClient(ClientHandler clientHandler) {
        unregisterClients.accept(clientHandler);
    }
}
