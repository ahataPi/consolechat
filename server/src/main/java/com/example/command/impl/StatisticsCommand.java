package com.example.command.impl;

import com.example.client.ClientHandler;
import com.example.command.Command;
import com.example.message.Message;
import com.example.msgsender.facade.MessageSenderFacade;

public class StatisticsCommand extends Command {

    public StatisticsCommand(String commandCode) {
        super(commandCode);
    }

    @Override
    public void execute(Message message, MessageSenderFacade messageSenderFacade) {
        ClientHandler client = message.getAuthor();
        messageSenderFacade.sendStatisticsMessage(client);
    }
}
