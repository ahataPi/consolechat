package com.example.handler;

import com.example.client.ClientHandler;
import com.example.client.ClientInfo;
import com.example.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;

public class InactivityHandler implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(InactivityHandler.class);

    private final Server server;

    public InactivityHandler(Server server) {
        this.server = server;
    }

    @Override
    public void run() {
        LOGGER.info("The inactivity handler is running...");
        while (server.isKeepRunning()) {
            if (server.getClients() != null && !server.getClients().isEmpty()) {
                for (ClientHandler client : server.getClients()) {
                    ClientInfo clientInfo = client.getClientInfo();
                    long difference = Duration.between(clientInfo.getLastMessageTime(), LocalDateTime.now()).getSeconds();
                    if (difference >= server.getInactivityTimeout()) {
                        LOGGER.info("The user {} will be disconnected by the server, because of inactivity.", clientInfo.getID());
                        client.disconnect();
                        server.unregisterClient(client);
                    }
                }
            }
        }
    }
}
