package com.example.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

public class SocketServerConnector {
    private static final Logger LOGGER = LoggerFactory.getLogger(SocketServerConnector.class);

    private Socket socket;
    private PrintWriter out;
    private BufferedReader in;

    public void connect(int port, String host) throws IOException {
        socket = new Socket(host, port);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
    }

    public void sendMessage(String message) {
        out.println(message);
    }

    public String readMessage() throws IOException {
        String message = null;
        try {
            if (in.ready()) {
                message = in.readLine();
            }
        } catch (IOException e) {
            LOGGER.error("The exception was thrown while reading messages from server: ", e);
        }
        return message;
    }

    public void disconnect() {
        stopSending();
        stopReading();
        closeSocket();
    }

    public void stopSending() {
        if (out != null) {
            out.close();
        }
    }

    public void stopReading() {
        try {
            if (in != null) {
                in.close();
            }
        } catch (IOException e) {
            LOGGER.error("The exception was thrown while closing the input stream: {}", e.getMessage());
        }
    }

    public void closeSocket() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                LOGGER.error("The exception was thrown while closing the socket: {}", e.getMessage());
            }
        }
    }
}
