package com.example.msgsender.commandmsgsender;

import com.example.client.ClientHandler;
import com.example.command.factory.CommandFactory;
import com.example.msgsender.MessageSender;

public interface HelpCommandMessageSender extends MessageSender {
    String HELP_COMMAND_MESSAGE = "You can type simple message to chat with other clients, or" +
            "Here is a list of available chat's commands is: \n %s";

    default void sendHelpMessage(ClientHandler clientHandler) {
        sendMessage(clientHandler, String.format(HELP_COMMAND_MESSAGE, CommandFactory.returnAvailableCommands()));
    }
}
