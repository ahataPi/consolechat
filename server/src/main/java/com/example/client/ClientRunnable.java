package com.example.client;

import com.example.executor.CommandExecutor;
import com.example.msgsender.facade.MessageSenderFacade;

import java.net.Socket;
import java.util.function.Consumer;

public class ClientRunnable implements Runnable {
    private final ClientHandler clientHandler;
    private final MessageSenderFacade messageSenderFacade;
    private final Consumer<ClientHandler> closeCallback;

    public ClientRunnable(Socket socket, MessageSenderFacade messageSenderFacade, Consumer<ClientHandler> closeCallback) {
        this.clientHandler = new ClientHandler(socket);
        this.messageSenderFacade = messageSenderFacade;
        this.closeCallback = closeCallback;
    }

    @Override
    public void run() {
        messageSenderFacade.confirmConnection(clientHandler);
        CommandExecutor commandExecutor = new CommandExecutor(clientHandler, messageSenderFacade);
        while (!clientHandler.getClientSocket().isClosed()) {
            String message = clientHandler.readMessage();
            if (message != null && !message.trim().isEmpty()) {
                commandExecutor.executeCommand(message);
            }
        }
        close();
    }

    private void close() {
        closeCallback.accept(clientHandler);
        messageSenderFacade.notifyDisconnectionOfClient(clientHandler);
    }

    public ClientHandler getClientHandler() {
        return clientHandler;
    }
}
