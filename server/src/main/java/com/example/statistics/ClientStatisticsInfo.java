package com.example.statistics;

import com.example.client.ClientInfo;

import java.time.LocalDateTime;

public class ClientStatisticsInfo {
    private final String nickname;
    private final long messageNumber;
    private final LocalDateTime lastActivityDate;

    public ClientStatisticsInfo(ClientInfo clientInfo) {
        this.nickname = clientInfo.getNickName();
        this.messageNumber = clientInfo.getMessagesNumber();
        this.lastActivityDate = clientInfo.getLastMessageTime();
    }

    @Override
    public String toString() {
        return "nickname='" + nickname + '\'' +
                ", messageNumber=" + messageNumber +
                ", lastActivityDate=" + lastActivityDate +
                '\n';
    }
}
