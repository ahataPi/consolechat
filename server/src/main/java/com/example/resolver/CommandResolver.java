package com.example.resolver;

import com.example.Constants;
import com.example.client.ClientHandler;
import com.example.command.factory.CommandFactory;
import com.example.msgsender.facade.MessageSenderFacade;

public class CommandResolver {
    private final ClientHandler clientHandler;
    private final MessageSenderFacade messageSenderFacade;

    public CommandResolver(ClientHandler clientHandler, MessageSenderFacade messageSenderFacade) {
        this.clientHandler = clientHandler;
        this.messageSenderFacade = messageSenderFacade;
    }

    public boolean isCommandMessage(String textMessage) {
        String potentialCommand = getCommandWord(textMessage);
        return potentialCommand.startsWith(Constants.COMMAND_START_SYMBOL);
    }

    public CommandFactory resolveCommandCode(String textMessage) {
        String potentialCommand = getCommandWord(textMessage);
        CommandFactory command = null;
        try {
            command = CommandFactory.valueOf(potentialCommand.toUpperCase().substring(1));
        } catch (IllegalArgumentException e) {
            messageSenderFacade.notifyNotFoundCommand(clientHandler);
        }
        return command;
    }

    private String getCommandWord(String textMessage) {
        String[] messageParts = textMessage.split(" ", 2);
        return messageParts[0];
    }
}
