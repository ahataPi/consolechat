package com.example.client;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class ClientInfo {
    private final UUID ID = UUID.randomUUID();
    private String nickName;
    private LocalDateTime lastMessageTime = LocalDateTime.now();
    private long messagesNumber = 0;
    private boolean loggedIn = false;

    public UUID getID() {
        return ID;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public LocalDateTime getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(LocalDateTime lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public long getMessagesNumber() {
        return messagesNumber;
    }

    public void incrementMessageNumber() {
        messagesNumber++;
    }
    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientInfo that = (ClientInfo) o;
        return Objects.equals(ID, that.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID);
    }
}
