package com.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Constants {
    public static final int DEFAULT_SERVER_PORT = 9999;
    public static final String DEFAULT_SERVER_HOST = "localhost";
    public static final String COMMAND_START_SYMBOL = "/";
    public static final long ACCEPTABLE_INACTIVITY_DURATION_SEC = 60;
    public static final ExecutorService DEFAULT_EXECUTOR_SERVICE = Executors.newCachedThreadPool();
    public static final int DEFAULT_TERMINATION_TIMEOUT_MS = 120000;
}
