package com.example.msgsender.commandmsgsender;

import com.example.client.ClientHandler;
import com.example.message.Message;
import com.example.msgsender.MessageSender;

public interface LoginCommandMessageSender extends MessageSender {
    String GREETING_OF_NEW_CLIENT_MESSAGE = "Let's greet our new client, %s!";
    String CHANGE_NICKNAME_MESSAGE = "The nickname '%s' has been already reserved or is not allowable, please login with another nickname!";
    String ALREADY_LOGGED_IN_MESSAGE = "You are already logged in with nickname '%s'. Use /help command to see what you can do in chat.";
    String SUCCESSFULLY_LOGGED_IN_MESSAGE = "You are successfully logged in with nickname '%s'. Use /help command to see what you can do in chat.";

    default void greetNewClient(ClientHandler clientHandler) {
        Message message = new Message(String.format(GREETING_OF_NEW_CLIENT_MESSAGE, clientHandler.getClientInfo().getNickName()), clientHandler);
        broadcastMessage(message);
    }

    default void warnChangeNickname(ClientHandler clientHandler, String potentialNickname) {
        String message = String.format(CHANGE_NICKNAME_MESSAGE, potentialNickname);
        sendMessage(clientHandler, message);
    }

    default void sendAlreadyLoggedIn(ClientHandler clientHandler) {
        String message = String.format(ALREADY_LOGGED_IN_MESSAGE, clientHandler.getClientInfo().getNickName());
        sendMessage(clientHandler, message);
    }

    default void congratsWithRegistration(ClientHandler clientHandler) {
        String message = String.format(SUCCESSFULLY_LOGGED_IN_MESSAGE, clientHandler.getClientInfo().getNickName());
        sendMessage(clientHandler, message);
    }
}
