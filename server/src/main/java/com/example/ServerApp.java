package com.example;

import com.example.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerApp {
    private static final Logger logger = LoggerFactory.getLogger(ServerApp.class);

    public static void main(String[] args) {
        final Server server = new Server.ServerBuilder().build();
        logger.info("The server is starting on the port {} (inactivity timeout {} s, termination timeout {} ms)",
                server.getPort(), server.getInactivityTimeout(), server.getTerminationTimeout());
        server.start();
    }
}