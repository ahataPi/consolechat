package com.example.command.impl;

import com.example.command.Command;
import com.example.message.Message;
import com.example.msgsender.facade.MessageSenderFacade;

public class HelpCommand extends Command {
    public HelpCommand(String commandCode) {
        super(commandCode);
    }

    @Override
    public void execute(Message message, MessageSenderFacade messageSenderFacade) {
        messageSenderFacade.sendHelpMessage(message.getAuthor());
    }
}