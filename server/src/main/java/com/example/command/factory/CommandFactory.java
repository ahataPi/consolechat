package com.example.command.factory;

import com.example.command.Command;
import com.example.command.impl.HelpCommand;
import com.example.command.impl.LoginCommand;
import com.example.command.impl.LogoutCommand;
import com.example.command.impl.StatisticsCommand;

public enum CommandFactory {
    LOGIN(new LoginCommand("/login [nickname]")),
    LOGOUT(new LogoutCommand("/logout")),
    STATS(new StatisticsCommand("/stats")),
    HELP(new HelpCommand("/help"));

    private final Command command;

    CommandFactory(Command command) {
        this.command = command;
    }

    public static String returnAvailableCommands() {
        StringBuilder stringBuilder = new StringBuilder();
        for (CommandFactory commandFactory : CommandFactory.values()) {
            stringBuilder.append(commandFactory.getCommand().getCommandCode());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public Command getCommand() {
        return command;
    }
}
