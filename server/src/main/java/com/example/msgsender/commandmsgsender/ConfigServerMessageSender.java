package com.example.msgsender.commandmsgsender;

import com.example.client.ClientHandler;
import com.example.msgsender.MessageSender;

public interface ConfigServerMessageSender extends MessageSender {
    String CONFIRMATION_OF_CONNECTION_MESSAGE = "Welcome to Server! You are now connected!";
    String NOT_FOUND_COMMAND_MESSAGE = "There is no such kind of command," +
            " use /help command to see the list of available commands.";

    default void confirmConnection(ClientHandler clientHandler) {
        sendMessage(clientHandler, CONFIRMATION_OF_CONNECTION_MESSAGE);
    }

    default void notifyNotFoundCommand(ClientHandler clientHandler) {
        sendMessage(clientHandler, NOT_FOUND_COMMAND_MESSAGE);
    }
}
