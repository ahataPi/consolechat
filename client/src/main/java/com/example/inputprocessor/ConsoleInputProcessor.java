package com.example.inputprocessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleInputProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleInputProcessor.class);

    private final BufferedReader bufferedReader;

    public ConsoleInputProcessor() {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    }

    public String readMessage() {
        String message = null;
        try {
            if (bufferedReader.ready()) {
                message = bufferedReader.readLine();
            }
        } catch (IOException e) {
            LOGGER.error("The exception was thrown while reading messages from client console: ", e);
        }
        return message;
    }
}
